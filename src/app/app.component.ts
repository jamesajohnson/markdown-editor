import { Component } from '@angular/core';

import { GdriveService } from './services/gdrive.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [GdriveService],
})
export class AppComponent {

}
