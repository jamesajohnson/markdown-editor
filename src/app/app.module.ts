import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

//pipes
import { MarkdownPipe } from './pipes/markdown.pipe';
import { IsMarkdownPipe } from './pipes/is-markdown.pipe';

//components
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { EditorComponent } from './components/editor/editor.component';
import { EditorTopbarComponent } from './components/editor-topbar/editor-topbar.component';
import { HomeNavbarComponent } from './components/home-navbar/home-navbar.component';
import { HomeTemplatesComponent } from './components/home-templates/home-templates.component';
import { HomeFilesComponent } from './components/home-files/home-files.component';
import { OrderFilesPipe } from './pipes/order-files.pipe';

const appRoutes: Routes = [
  { path: 'editor', component: EditorComponent },
  { path: 'editor/:id', component: EditorComponent },
  { path: '', component: HomeComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MarkdownPipe,
    HomeComponent,
    EditorComponent,
    EditorTopbarComponent,
    HomeNavbarComponent,
    HomeTemplatesComponent,
    HomeFilesComponent,
    IsMarkdownPipe,
    OrderFilesPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
