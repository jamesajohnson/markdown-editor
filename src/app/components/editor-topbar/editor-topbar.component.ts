import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'editor-topbar',
  templateUrl: './editor-topbar.component.html',
  styleUrls: ['./editor-topbar.component.scss']
})
export class EditorTopbarComponent implements OnInit {

  @Output() saveFileContent = new EventEmitter();
  @Output() downloadFileLocally = new EventEmitter();
  @Input() fileName: string;

  pageTitle = 'Page Title';

  constructor() { }

  ngOnInit() {
  }

  save() {
    this.saveFileContent.emit({
      value: true
    });
  }

  download() {
    this.downloadFileLocally.emit({
      value: true
    });
  }

}
