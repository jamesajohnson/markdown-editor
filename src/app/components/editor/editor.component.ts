import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

//services
import { GdriveService } from '../../services/gdrive.service';

// pipes
import { MarkdownPipe } from '../../pipes/markdown.pipe';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {

  private fileName :string = "Document Name";
  private markdown :string = '### your markdown code';

  //Google Drive
  private driveFileId :string;
  private fileMeta :Object; //file information object

  constructor(
    private gdriveService: GdriveService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    //get ID from route
    this.route.params.forEach((params: Params) => {
      if (params['id'] !== undefined) {
        this.driveFileId = params['id'];

        //get file info
        this.gdriveService.getFileMeta(this.driveFileId).then((file) => {
          this.fileMeta = file;
          this.fileName = file.name;
          this.downloadFile(this.driveFileId);
        });
      } else {

      }
    });
  }

  downloadFile(id :string) :void {
    this.gdriveService.downloadFile(id).then((data) => {
      console.log(data);
      this.markdown = data._body;
    });
  }

  /*
  * Download the .md file
  */
  downloadFileLocally() :void {
    this.gdriveService.downloadFileLocally(this.fileMeta.webContentLink);
  }

  /*
  * Save button clicked, should I update or create new
  */
  saveFileContent() :void {
    this.gdriveService.getAuth().then((hasAccess) => {
      if( hasAccess ) {
        if ( this.driveFileId ){
          console.log("updating file");
          this.updateFile();
        } else {
          console.log("saving new file");
          this.saveNewFile();
        }
      }
    })
  }

  saveNewFile() :void {
    this.gdriveService.createFile(this.fileName, this.markdown).then((data) => {
      //console.log(data);
      if (data.status == 200) {
        //TODO pretty alert
        window.alert("New File Saved To Drive");
      }
    });
  }

  updateFile() :void {
    this.gdriveService.updateFile(this.driveFileId, this.markdown).then((data) => {
      console.log(data);
      if (data.status == 200) {
        //TODO pretty alert
        window.alert("File Saved To Drive");
      }
    });
  }

}
