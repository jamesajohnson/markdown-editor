import {
  Component,
  OnInit,
  trigger,
  state,
  style,
  transition,
  animate
} from '@angular/core';
import { Router } from '@angular/router';

//services
import { GdriveService } from '../../services/gdrive.service';

//pipes
import { IsMarkdownPipe } from '../../pipes/is-markdown.pipe';
import { OrderFilesPipe } from '../../pipes/order-files.pipe';

@Component({
  selector: 'home-files',
  templateUrl: './home-files.component.html',
  styleUrls: ['./home-files.component.scss'],
  animations: [
    trigger('visibility', [
      state('shown', style({
        opacity: 1
      })),
      state('hidden', style({
        opacity: 0
      })),
      transition('* => *', animate('1s'))
    ])
  ]
})
export class HomeFilesComponent implements RouteReuseStrategy, OnInit {

  //boolean values
  private connectedToDrive :boolean = false;
  private hasUserInfo :boolean = false;

  //data objects
  private userInfo :Object = [];
  private firstName :string = 'Your';
  private files :Object = [];
  private filterOptions :Object[];
  private selectedFilter :Object;

  constructor(
    private router: Router,
    private gdriveService: GdriveService
  ) {
    // options to filter files by
    this.filterOptions = [
      { name: 'Title', value: 'title' },
      { name: 'Date created', value: 'created' },
      { name: 'Last opened', value: 'last-opened' },
      { name: 'Last modified', value: 'last-modified' },
      { name: 'Last modified by me', value: 'last-modified-by-me' }
    ];
    this.selectedFilter = this.filterOptions[0];
  }

  ngOnInit() {
    if(this.gdriveService.hasAuth()) {
      this.connectedToDrive = true;

      this.driveAbout();
      this.driveChanges();
      this.driveListFiles();
    }
  }

  /*
  * Reorder the files list when the user selects a filterOptions
  */
  updateSelectedValue(event:string): void{
    this.selectedFilter = JSON.parse(event);
  }

  /*
  * Connect to the google drive serive and request access
  */
  driveConnect() :void {
    this.gdriveService.getAuth().then((hasAccess) => {
      console.log(hasAccess);
      if( hasAccess ) {
        this.connectedToDrive = true;
        this.driveAbout();
        this.driveChanges();
        this.driveListFiles();
      }
    });
  }

  /**
  * Get information about the user & their account
  */
  driveAbout() :void {
    this.gdriveService.getAbout().then((info) => {
      this.hasUserInfo = true;
      this.userInfo = info;
      this.firstName = info.user.displayName.split(" ")[0];

      //Add s or ' to display name
      if (this.firstName.endsWith("s")) {
        this.firstName += "'";
      } else {
        this.firstName += "s";
      }

      console.log(this.firstName);
    });
  }

  /**
  * Get list of changes
  */
  driveChanges() :void {
    this.gdriveService.getChanges().then((changes) => {
    });
  }

  /*
  * List files in google drive
  */
  driveListFiles() :void {
    this.gdriveService.listFiles().then((files) => {
      this.files = files;
      console.log(files);
    });
  }

  /*
  * Open file in editor component
  */
  openFileInEditor(id :String) :void {
    let link = ['/editor', id];
    this.router.navigate(link);
  }
}
