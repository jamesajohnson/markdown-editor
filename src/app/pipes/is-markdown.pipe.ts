import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isMarkdown'
})
export class IsMarkdownPipe implements PipeTransform {

  /**
  * Return files from list of files where mimeType is markdown
  * @return {Object[]} list of files where mimeType is markdown
  */
  transform(value: Object[], args?: any): any {
    return value.filter(file => file.mimeType == 'text/markdown');
  }

}
