import { Pipe, PipeTransform } from '@angular/core';
import * as marked from 'marked';

@Pipe({
  name: 'markdown'
})
export class MarkdownPipe implements PipeTransform {

  transform(value: string, args?: any): any {
    var md = marked.setOptions(args);
    return md.parse(value);
  }

}
