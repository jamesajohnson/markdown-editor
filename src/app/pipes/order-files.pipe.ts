import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderFiles'
})
export class OrderFilesPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    console.log('reordering values of:');
    console.log(value);
    switch(args.value)
    {
      case 'title':
        return value.sort((n1,n2) => {
          if (n1.name < n2.name)
            return 1;
          else
            return -1;
        });
      case 'last-opened':
        return value.sort((n1,n2) => {
          if (n1.viewedByMeTime < n2.viewedByMeTime)
            return 1;
          else
            return -1;
        });
      case 'last-modified':
        return value.sort((n1,n2) => {
          if (n1.modifiedTime < n2.modifiedTime)
            return 1;
          else
            return -1;
        });
      case 'last-modified-by-me':
        return value.sort((n1,n2) => {
          if (n1.modifiedByMeTime < n2.modifiedByMeTime)
            return 1;
          else
            return -1;
        });
      case 'created':
        return value.sort((n1,n2) => {
          if (n1.createdTime < n2.createdTime)
            return 1;
          else
            return -1;
        });
    }
    return value;
  }

}
