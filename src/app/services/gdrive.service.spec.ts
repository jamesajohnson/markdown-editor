/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GdriveService } from './gdrive.service';

describe('Service: Gdrive', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GdriveService]
    });
  });

  it('should ...', inject([GdriveService], (service: GdriveService) => {
    expect(service).toBeTruthy();
  }));
});
