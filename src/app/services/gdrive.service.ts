import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'

const CLIENT_ID = '175783309819-glinpd4fvau3c85f5tmcifal3dufge7f.apps.googleusercontent.com';
const urlAuth = "https://accounts.google.com/o/oauth2/auth?";

@Injectable()
export class GdriveService {

  private drive :Object;              //Google drive api

  private token :String;              //auth token
  public authorised :boolean = false;    //is authorised
  private filesRequest :Object;       //results of files request

  constructor(public http: Http) {

  }

  /**
  * Get authorization
  * @return {boolean} hasAccess Does the api have access to users drive
  */
  getAuth() :Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      gapi.auth.authorize(
        {
          client_id: '175783309819-glinpd4fvau3c85f5tmcifal3dufge7f.apps.googleusercontent.com',
          scope: ['https://www.googleapis.com/auth/drive'],
          immediate: false
        },
        resp => {
          if (resp && !resp.error) {
            console.log(resp);
            this.token = resp.access_token;
            this.authorised = true;
            gapi.client.load('drive', 'v3', () => {
              resolve(true);
            });
          } else {
            reject(false);
          }
        }
      );
    });
  }

  hasAuth() :boolean {
    return this.authorised;
  }

  /**
  * return about information
  * @return {Object} about
  */
  getAbout() :Promise<Object[]> {
    let request = gapi.client.drive.about.get({
      fields: 'user/*,storageQuota/*'
    });

    return new Promise((resolve, reject) => {
      request.execute(resp => {
        console.log(resp);
        resolve(resp);
      });
    });
  }

  /**
  * return changes for user
  * @return {Object} changes
  */
  getChanges() :Promise<Object[]> {
    let request = gapi.client.drive.changes.list({
      'pageToken': 1
    });

    return new Promise((resolve, reject) => {
      request.execute(resp => {
        resolve(resp);
      });
    });
  }

  /**
  * return files in google drive
  * @return {Object[]} files All fiels in drive.
  */
  listFiles() :Promise<Object> {
    console.log("listing files");
    let request = gapi.client.drive.files.list({
      'pageSize': 10,
      'fields': "nextPageToken, files(id, name, mimeType, modifiedTime, modifiedByMeTime, viewedByMeTime, createdTime)"
    });

    return new Promise<Object[]>((resolve, reject) => {
      request.execute(resp => {
        resolve(resp.files);
      });
    });
  }

  /**
  * Generate get IDs for file requests
  */
  generateIDs() :Promise<String> {
    var request = gapi.client.drive.files.generateIds({

    });

    return new Promise<String>((resolve, reject)=> {
      request.execute(resp => {
        resolve(resp);
      });
    });
  }

  /**
  * Generate get IDs for file requests
  */
  getToken() :String {
    return this.token;
  }

  /*
  * Get and information of a file
  */
  getFileMeta(id :string) :Promise<Object> {
    var request = gapi.client.drive.files.get({
      'fileId': id,
      'fields': 'name, webContentLink'
    });

    return new Promise<Object>((resolve, reject) => {
      request.execute(resp => {
        resolve(resp);
      });
    });
  }

  /**
  * Download file from web content link
  * @return {Object} file
  */
  downloadFile(id :string) :Promise<Object> {
    let headers = new Headers({'Authorization': 'Bearer ' + this.token}); //generate headers
    let options = new RequestOptions({ headers: headers }); // Create a request option

    return this.http
    .get('https://www.googleapis.com/drive/v3/files/' + id + '?alt=media', options)
    .toPromise();
  }

  /**
  * Open the files webContentLink in a new tab
  * @return {Object} file
  */
  downloadFileLocally(url :string) {
    window.open(url);
  }

  /**
  * save the content to a new file
  * @return {Object} response
  */
  createFile(name :string, content? :string) :Promise<Object> {
    //create request
    if( content ) {
      let headers = new Headers({
        'Content-Type': 'text/markdown',
        'Authorization': 'Bearer ' + this.token,
        'title': name //TODO name not working!
      });
      let options = new RequestOptions({ headers: headers }); // Create a request option

      return this.http
        .post('https://www.googleapis.com/upload/drive/v3/files' + '?uploadType=multipart', content, options)
        .toPromise();
    } else {
      let headers = new Headers({
        'Content-Type': 'text/markdown',
        'Authorization': 'Bearer ' + this.token,
        'name': name //TODO name not working!
      });
      let options = new RequestOptions({ headers: headers }); // Create a request option

      return this.http
        .post('https://www.googleapis.com/drive/v3/files' + '?uploadType=metadata', options)
        .toPromise();
    }
  }

  /**
  * replace file content with param content
  * @return {Object} response
  */
  updateFile(id :string, content :string) :Promise<Object> {
    console.log("updating file");
    let headers = new Headers({
      'Content-Type': 'text/markdown',
      'Authorization': 'Bearer ' + this.token,
      'id': id
    }); //generate headers
    let options = new RequestOptions({ headers: headers }); // Create a request option

    return this.http
      .patch('https://www.googleapis.com/upload/drive/v3/files/' + id + '?uploadType=multipart', content, options)
      .toPromise();
  }
}
