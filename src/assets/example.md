# Markdown Editor

Allow you to:
- Load files from Google Drive,
- Edit markdown code,
- View live changes to documents,
- Save .md file to Google Drive.
